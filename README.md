# ESP8266-VK-API

Простой IoT проект управления парой реле с помощью ESP8266 через vk-api чат-бота с клавиатурой.

### Используемые компоненты:
#### VK-Чат-бот:
* Исходник и инструкция в папке "web-server"
* Python версии 3.5+
* Хостинг [Pythonanywhere](https://www.pythonanywhere.com/)
    * [Статья](https://habr.com/ru/post/144420/) о нём на Хабре
* Фреймворк [Flask](http://flask.pocoo.org/)
    * Почитать можно [тут](https://habr.com/ru/post/346306/)
* Библиотека Python vk.com API wrapper
    * [Страничка на pypi.org](https://pypi.org/project/vk/)
    * [Репозиторий на Github](https://github.com/voronind/vk)
    * [Документация](https://vk.readthedocs.io/en/latest/)
    * [Полезная статья на Хабре](https://habr.com/ru/post/326898/)
    
#### Контроллер:
* Прошивка и инструкция в папке "esp8266-firmware"
* NodeMCU v3 LoLin (ESP8266)
    * [Статья](http://robotclass.ru/articles/node-mcu-arduino-ide-setup/)
    как начать работать с данной платой в  Arduino IDE
* [Arduino IDE](https://www.arduino.cc/en/Main/Software) версии 1.8.6+
* Библиотеки:
    * [ArduinoJson](https://arduinojson.org/) - 
    [Документация](https://arduinojson.org/v6/doc/deserialization/) по десериализации
    * [Esp8266](https://github.com/esp8266/Arduino) by ESP8266 Community