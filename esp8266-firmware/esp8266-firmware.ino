#include <Arduino.h>
#include <ArduinoJson.h>

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>

#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

ESP8266WebServer server(8818);
ESP8266WiFiMulti WiFiMulti;
WiFiClient client;
HTTPClient http;
String extractorState;

int fanRelay = D5;
int lampRelay = D6;

boolean lastFanState = false;
boolean lastLampState = false;

const int capacity = JSON_OBJECT_SIZE(10);
StaticJsonDocument<capacity> extractorStateObj;

void handleRoot() {
  Serial.println("Получен входящий запрос");
  server.send(200, "text/plain", "hello from esp8266!");
}

void handleNotFound() {
  Serial.println("Получен входящий запрос на несуществующую страницу");
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

String checkExtractorState(){
  if ((WiFiMulti.run() == WL_CONNECTED)) {

    if (http.begin(client, "http://serjp.pythonanywhere.com/")) {  // HTTP

      int httpCode = http.GET();
      if (httpCode > 0) {
        if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
          String payload = http.getString();
          DeserializationError err = deserializeJson(extractorStateObj, payload);
          if (err) {
            Serial.print(F("deserializeJson() failed with code "));
            Serial.println(err.c_str());
          } else {
            return "200";
          }
        }
      } else {
        Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
        return "err";
      }
      http.end();
      
    } else {
      Serial.printf("[HTTP} Unable to connect\n");
      return "err";
    }
  }
}

bool switchState(bool state, bool lastState, int pin){
  if(state != lastState){
    switch(state){
    case true:
      digitalWrite(pin, LOW);
      Serial.println("Switch state to LOW");
      Serial.print("Current stateis: ");
      Serial.println(digitalRead(pin));
      return true;
      break;
    case false:
      digitalWrite(pin, HIGH);
      Serial.println("Switch state to HIGH");
      Serial.print("Current stateis: ");
      Serial.println(digitalRead(pin));
      return false;
      break;
    }
  } else {
    return lastState;
  }
}

void setup() {
  Serial.begin(9600);
  // Serial.setDebugOutput(true);

  Serial.println();
  Serial.println();
  Serial.println();

  pinMode(fanRelay, OUTPUT);
  digitalWrite(fanRelay, HIGH);
  pinMode(lampRelay, OUTPUT);
  digitalWrite(lampRelay, HIGH);

  for (uint8_t t = 4; t > 0; t--) {
    Serial.printf("[SETUP] WAIT %d...\n", t);
    Serial.flush();
    delay(1000);
  }

  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP("ASUS_5C", "2P7C9M44");

  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }

  server.on("/", handleRoot);

  server.on("/inline", []() {
    server.send(200, "text/plain", "this works as well");
  });

  server.on("/state", []() {
    String output = "";
    serializeJson(extractorStateObj, output);
    server.send(200, "text/plain", output);
  });

  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");

}

void loop() {
  
  server.handleClient();
  MDNS.update();

  if(checkExtractorState() != "err"){
    boolean fanState = extractorStateObj["fan"]["state"];
    boolean lampState = extractorStateObj["lamp"]["state"];
    lastFanState = switchState(fanState, lastFanState, fanRelay);
    lastLampState = switchState(lampState, lastLampState, lampRelay);
  } else {
    Serial.println("Error whith http request");
  }

  delay(10000);

}
