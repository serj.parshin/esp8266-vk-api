from flask import Flask, request, json
from kithen import kithenExtract
from settings import *
import vk, random

app = Flask(__name__)
kithenExtractor = kithenExtract()
global lastJson

@app.route('/')
def hello_world():
    return kithenExtractor.toJSON()

@app.route('/lastJson')
def getLastJson():
    global lastJson
    return json.dumps(lastJson, default=lambda o: o.__dict__,
            sort_keys=True, indent=4)

@app.route('/', methods=['POST'])
def processing():
    #Распаковываем json из пришедшего POST-запроса
    data = json.loads(request.data)
    global lastJson
    lastJson = data
    #Вконтакте в своих запросах всегда отправляет поле типа
    if 'type' not in data.keys():
        return 'not vk'
    if (data['secret'] == secret_key):
        if data['type'] == 'confirmation':
            return confirmation_token
        elif data['type'] == 'message_new':
            session = vk.Session()
            api = vk.API(session, v=5.95)
            user_id = data['object']['from_id']
            random_int = random.randint(0, 65535)

            if data['object']['text'] == 'start':
                api.messages.send(access_token=token, user_id=str(user_id),
                                  random_id=str(random_int),
                                  message='Привет',
                                  keyboard=open("/home/serjp/mysite/keyboard_one.json","r",encoding="UTF-8").read())
                # Сообщение о том, что обработка прошла успешно
                return 'ok'

            if 'payload' in data['object']:
                load = json.loads(data['object']['payload'])
                if 'Fan' in load:
                    if load['Fan'] == 'True':
                        api.messages.send(access_token=token,
                                          user_id=str(user_id),
                                          random_id=str(random_int),
                                          message='Включаю вытяжку',
                                          keyboard=open("/home/serjp/mysite/keyboard_one.json","r",encoding="UTF-8").read())
                        kithenExtractor.fan.setState(True)
                        return 'ok'
                    if load['Fan'] == 'False':
                        api.messages.send(access_token=token,
                                          user_id=str(user_id),
                                          random_id=str(random_int),
                                          message='Выключаю вытяжку',
                                          keyboard=open("/home/serjp/mysite/keyboard_one.json","r",encoding="UTF-8").read())
                        kithenExtractor.fan.setState(False)
                        return 'ok'
                if 'Lamp' in load:
                    if load['Lamp'] == 'True':
                        api.messages.send(access_token=token,
                                          user_id=str(user_id),
                                          random_id=str(random_int),
                                          message='Включаю свет',
                                          keyboard=open("/home/serjp/mysite/keyboard_one.json","r",encoding="UTF-8").read())
                        kithenExtractor.lamp.setState(True)
                        return 'ok'
                    if load['Lamp'] == 'False':
                        api.messages.send(access_token=token,
                                          user_id=str(user_id),
                                          random_id=str(random_int),
                                          message='Выключаю свет',
                                          keyboard=open("/home/serjp/mysite/keyboard_one.json","r",encoding="UTF-8").read())
                        kithenExtractor.lamp.setState(False)
                        return 'ok'
    else:
        return 'bye'