import json

class kithenPart:
    def __init__(self, name, state):
        """Constructor"""
        self.name = name
        self.state = state

    def setState(self, state):
        self.state = state

    def getState(self):
        return self.state

    def getName(self):
        return self.name

class kithenExtract:
    def __init__(self):
        """Constructor"""
        self.fan = kithenPart('Fan', False)
        self.lamp = kithenPart('Lamp', False)

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
            sort_keys=True, indent=4)